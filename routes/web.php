<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/no-script', function () {
    return view('no-script');
});*/

Route::get('/', 'HomeController@getHome');

Route::get('/images/{folder}/{name?}', 'ImageController@getImage');
Route::get('/images/{folder}/{id}/{name}', 'ImageController@getImage');
Route::get('/account-verify/{token?}', 'VerifyAccountController@verifyAccount');


