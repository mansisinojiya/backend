<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Admin\ForgotPasswordMail' => [
            'App\Handlers\Events\Admin\SendForgotPasswordMail',
        ],

        'App\Events\User\AccountVerificationMail' => [
            'App\Handlers\Events\User\SendAccountVerificationMail',
        ],

        'App\Events\User\UserForgotPasswordMail' => [
            'App\Handlers\Events\User\SendForgotPasswordLinkMail',
        ],

        'App\Events\User\UserResetPasswordMail' => [
            'App\Handlers\Events\User\SendResetPasswordMail',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        //
    }
}
