<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Hobby
 *
 * @property int $id
 * @property string $hobby
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Hobby newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Hobby newQuery()
 * @method static \Illuminate\Database\Query\Builder|Hobby onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Hobby query()
 * @method static \Illuminate\Database\Eloquent\Builder|Hobby whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hobby whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hobby whereHobby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hobby whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hobby whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Hobby withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Hobby withoutTrashed()
 * @mixin \Eloquent
 */
class Hobby extends Model
{
    use SoftDeletes;
    protected $table = 'hobby';
}
