<?php

namespace App\Events\Admin;

use App\Models\Admin;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ForgotPasswordMail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $first_name;
    public $last_name;
    public $email;
    public $otp;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Admin $adminUser)
    {
        $this->first_name = $adminUser->first_name;
        $this->last_name = $adminUser->last_name;
        $this->email = $adminUser->email;
        $this->otp = $adminUser->otp;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
