<?php

namespace App\Events\User;

use App\Models\User;
use App\Models\VerifyToken;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UserForgotPasswordMail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $name;
    public $email;
    public $otp;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param VerifyToken $verifyToken
     */
    public function __construct(User $user, VerifyToken $verifyToken)
    {
        $this->name = $user->name;
        $this->email = $user->email;
        $this->otp = $verifyToken->token;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
