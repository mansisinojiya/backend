<?php

namespace App\Events\User;

use App\Models\User;
use App\Models\VerifyToken;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class AccountVerificationMail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $name;
    public $email;
    public $link;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param VerifyToken $verifyToken
     */
    public function __construct(User $user, VerifyToken $verifyToken)
    {
        $this->email = $user->email;
        $this->name = $user->name;
        $this->link = config('api.api_url').'/account-verify?token='.$verifyToken->token;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
