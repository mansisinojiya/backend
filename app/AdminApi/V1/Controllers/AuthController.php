<?php

namespace App\AdminApi\V1\Controllers;

use App\AdminApi\AdminApiController;
use App\AdminApi\V1\Resources\Auth\AdminAuthResource;
use App\Events\Admin\ForgotPasswordMail;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends AdminApiController
{
    public function login(Request $request)
    {
        $this->validateRequest('login');
        $requestParam = $request->all();
        $admin = Admin::whereEmail(strtolower($requestParam['email']))->first();
        if ($admin && Hash::check($requestParam['password'], $admin->password)) {
            $token = $this->generateAccessToken($requestParam);
            return AdminAuthResource::make($admin)->additional(['meta' => [
                'message' => __('admin_api.loginSuccess'),
                'code' => AdminApiController::$SUCCESS,
                'token' => $token,
            ]]);
        } else {
            return AdminApiController::unAuthenticatedResponse('admin_api.wrongCredentials');
        }
    }

    public function forgotPassword(Request $request)
    {
        $requestParam = $request->only('email');
        $this->validateRequest('forgotPassword');
        $admin = Admin::whereEmail(strtolower($requestParam['email']))->first();
        if ($admin) {
            $otp = self::generateOtpAdmin();
            $admin->otp = $otp;
            $admin->save();
            event(new ForgotPasswordMail($admin));
            return AdminApiController::successFailResponse(null, 'admin_api.forgetPasswordLink', AdminApiController::$SUCCESS);
        } else return AdminApiController::unAuthenticatedResponse('admin_api.emailNotExist');
    }

    public function resetPassword(Request $request)
    {
        $requestParam = $request->only('password', 'otp');
        $this->validateRequest('resetPassword');
        $admin = Admin::whereOtp($requestParam['otp'])->first();
        if (!$admin) return AdminApiController::successFailResponse(null, 'admin_api.invalid_otp', AdminApiController::$FAIL);
        if (($admin && !$admin->otp)) return AdminApiController::successFailResponse(null, 'admin_api.otp_expired', AdminApiController::$FAIL);
        if ($admin) {
            $admin->password = Hash::make($requestParam['password']);
            $admin->otp = null;
            $admin->save();
            return AdminApiController::successFailResponse(null, 'admin_api.resetPasswordSuccess', AdminApiController::$SUCCESS);
        }
    }

    public function changePassword(Request $request)
    {
        $requestParam = $request->only('old_password', 'password');
        $this->validateRequest('changePassword');
        $admin = \Auth::user();
        if (Hash::check($requestParam['old_password'], $admin->password)) {
            $admin->password = Hash::make($requestParam['password']);
            $admin->save();
            return AdminApiController::successFailResponse(null, 'admin_api.password_change_success', AdminApiController::$SUCCESS);
        }
        return AdminApiController::successFailResponse(null, 'admin_api.invalidOldPassword', AdminApiController::$FAIL);
    }

    public function getDetails(Request $request)
    {
        $admin = \Auth::user();
        return AdminAuthResource::make($admin)->additional(['meta' => [
            'message' => __('admin_api.success'),
            'code' => AdminApiController::$SUCCESS
        ]]);
    }

    public function editProfile(Request $request)
    {
        $this->validateRequest('editProfile');
        $admin = Admin::whereId(\Auth::user()->id)->first();
        $admin->first_name = $request->get('first_name');
        $admin->last_name = $request->get('last_name');
        $admin->mobile = $request->get('mobile');
        $admin->save();
        return AdminAuthResource::make($admin)->additional(['meta' => [
            'message' => __('admin_api.profile_update_success'),
            'code' => AdminApiController::$SUCCESS
        ]]);
    }
}
