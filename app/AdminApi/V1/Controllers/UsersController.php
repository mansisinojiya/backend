<?php

namespace App\AdminApi\V1\Controllers;

use App\AdminApi\AdminApiController;
use App\AdminApi\V1\Resources\Roles\RolesResource;
use App\AdminApi\V1\Resources\Users\UsersResource;
use App\AdminApi\V1\Resources\Users\VerifiedAccountRequestResuorce;
use App\Models\Role;
use App\Models\User;
use App\Models\VerifiedAccountRequest;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UsersController extends AdminApiController
{

    public function getList(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : self::$DEFAULT_PER_PAGE;
        $order_by = $request->get('order_by') ? $request->get('order_by') : self::$DEFAULT_ORDERBY_COLUMN;
        $direction = $request->get('direction') ? $request->get('direction') : self::$DEFAULT_ORDERBY_DIRECTION;
        if ($request->has('direction') && $request->get('direction')) {
            $validator = \Validator::make($request->all(),
                ['direction' => 'in:asc,desc'],
                ['direction.in' => 'admin_api.invalid_orderby_direction']
            );
            if ($validator->fails()) return AdminApiController::validationResponse($validator);
        }
        $data = User::whereIsVerify(1)->orderBy($order_by, $direction)
            ->where(function ($query) use ($request) {
                if ($request->has('search')) {
                    $query->orWhere('official_name', 'like', '%' . $request->get('search', null) . '%');
                    $query->orWhere('email', 'like', '%' . $request->get('search', null) . '%');
                }
            })->paginate($per_page);
        return UsersResource::collection($data)->additional(['meta' => [
            'message' => __('admin_api.success'),
            'code' => AdminApiController::$SUCCESS
        ]]);
    }

    public function getDetails($id){
        $data = User::whereId($id)->first();
        if ($data){
            return UsersResource::make($data)->additional(['meta' => [
                'message' => __('admin_api.success'),
                'code' => AdminApiController::$SUCCESS
            ]]);
        } else return AdminApiController::successFailResponse(null, 'admin_api.invalid_user_id', AdminApiController::$FAIL);
    }

    public function deleteUser(Request $request){
        $data = User::whereId($request->get('id'))->first();
        if (!$data) return AdminApiController::successFailResponse(null, 'admin_api.invalid_user_id', AdminApiController::$FAIL);
        if ($data->delete()){
            return UsersResource::make($data)->additional(['meta' => [
                'message' => __('admin_api.success'),
                'code' => AdminApiController::$SUCCESS
            ]]);
        } else return AdminApiController::successFailResponse(null, 'admin_api.invalid_user_id', AdminApiController::$FAIL);
    }

    public function addUser(Request $request){
        
    }
}
