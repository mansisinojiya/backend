<?php

namespace App\AdminApi\V1\Resources\Users;

use App\Api\ApiController;
use App\Models\City;
use App\Models\Country;
use App\Models\Language;
use App\Models\Position;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;


class UsersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $documents = $this->documents;
        foreach ($documents as $value) {
            $value->document = $value->document ? url('/images/user/' .$this->id.'/'.$value->document) : '';
        }
        return [
            'id' => $this->id,
            'role_id' => $this->role_id,
            'role_name' => Role::whereId($this->role_id)->first()->name,
            'email' => $this->email,
            'official_name' => $this->official_name,
            'country_id' => $this->country_id,
            'country_name' => Country::whereId($this->country_id)->first() ? Country::whereId($this->country_id)->first()->name : '',
            'city_id' => $this->city_id,
            'city_name' => City::whereId($this->city_id)->first() ? City::whereId($this->city_id)->first()->name : '',
            'date_of_birth' => $this->date_of_birth ? ApiController::carbonDate($this->date_of_birth, 'Y-m-d') : null,
            'birth_place' => $this->birth_place,
            'country_origin_id' => $this->country_origin_id,
            'height' => $this->height,
            'weight' => $this->weight,
            'preferred_foot' => $this->preferred_foot,
            'position_id' => $this->position_id,
            'position_name' => Position::whereId($this->position_id)->first() ? Position::whereId($this->position_id)->first()->name : '',
            'current_club' => $this->current_club,
            'previous_clubs' => $this->previous_clubs,
            'status' => $this->status,
            'contract_ends' => $this->contract_ends ? ApiController::carbonDate($this->contract_ends, 'Y-m-d') : null,
            'nationality' => $this->nationality,
            'travel_passport' => $this->travel_passport,
            'language_id' => $this->language_id,
            'language_name' => Language::whereId($this->language_id)->first() ? Language::whereId($this->language_id)->first()->name : '',
            'phone' => $this->phone,
            'last_placed' => $this->last_placed,
            'fifa_number' => $this->fifa_number,
            'club_coach_details' => $this->club_coach_details,
            'club_name' => $this->club_name,
            'division' => $this->division,
            'website' => $this->website,
            'licence' => $this->licence,
            'account_verify' => $this->account_verify,
            'image' => $this->image ? url('/images/user/' . $this->image) : '',
            'facebook_id' => $this->facebook_id,
            'google_id' => $this->google_id,
            'instagram_id' => $this->instagram_id,
            'linkedin_id' => $this->linkedin_id,
            'firebase_id' => $this->firebase_id,
            'firebase_token' => $this->firebase_token,
            'device_id' => $this->device_id,
            'device_type' => $this->device_type,
            'created_at' => ApiController::carbonDate($this->created_at),
            'updated_at' => ApiController::carbonDate($this->updated_at),
            'achievements' => $this->achievements,
            'documents' => $documents,
        ];
    }
}
