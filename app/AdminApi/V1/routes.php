<?php
Route::group(['middleware' => ['api-logger'], 'prefix' => 'v1', 'namespace' => 'App\AdminApi\V1\Controllers'], function ($route) {

    Route::group(['middleware' => 'admin-api-auth'], function () {
        Route::post('/login', 'AuthController@login');
        Route::post('/forgot-password', 'AuthController@forgotPassword');
        Route::post('/reset-password', 'AuthController@resetPassword');
        Route::get('/check-subscription', 'SubscriptionController@checkSubscription');
        Route::group(['middleware' => ['auth:admin-api']], function () {
            Route::post('/change-password', 'AuthController@changePassword');
            Route::post('/edit-profile', 'AuthController@editProfile');
            Route::get('/get-details', 'AuthController@getDetails');

            //Country
            Route::post('/add-edit-country', 'CountryController@addEditCountry');
            Route::post('/countries', 'CountryController@getList');
            Route::get('/countries/{id}', 'CountryController@getDetails');
            Route::get('/get-all-countries', 'CountryController@getALLCountries');
            Route::get('/delete-country/{id}', 'CountryController@deleteCountry');

            //Roles
            Route::post('/add-edit-role', 'RolesController@addEditRoles');
            Route::post('/roles', 'RolesController@getList');
            Route::get('/roles/{id}', 'RolesController@getDetails');
            Route::get('/get-all-roles', 'RolesController@getALLRoles');
            Route::get('/delete-role/{id}', 'RolesController@deleteRoles');

            //Languages
            Route::post('/add-edit-language', 'LanguagesController@addEditLanguage');
            Route::post('/languages', 'LanguagesController@getList');
            Route::get('/languages/{id}', 'LanguagesController@getDetails');
            Route::get('/get-all-languages', 'LanguagesController@getALLLanguages');
            Route::get('/delete-languages/{id}', 'LanguagesController@deleteLanguage');

            //Position
            Route::post('/add-edit-position', 'PositionController@addEditPosition');
            Route::post('/positions', 'PositionController@getList');
            Route::get('/positions/{id}', 'PositionController@getDetails');
            Route::get('/get-all-positions', 'PositionController@getALLPositions');
            Route::get('/delete-position/{id}', 'PositionController@deletePosition');

            //SubPosition
            Route::post('/add-edit-sub-position', 'SubPositionController@addEditSubPosition');
            Route::post('/sub-positions', 'SubPositionController@getList');
            Route::get('/sub-positions/{id}', 'SubPositionController@getDetails');
            Route::get('/get-all-sub-positions', 'SubPositionController@getALLSubPositions');
            Route::get('/delete-sub-position/{id}', 'SubPositionController@deleteSubPosition');

            //City
            Route::post('/add-edit-city', 'CityController@addEditCity');
            Route::post('/cities', 'CityController@getList');
            Route::get('/cities/{id}', 'CityController@getDetails');
            Route::get('/get-all-cities', 'CityController@getALLCities');
            Route::get('/delete-city/{id}', 'CityController@deleteCity');

            //Users
            Route::post('/users', 'UsersController@getList');
            Route::get('/user/{id}', 'UsersController@getDetails');
            Route::post('/update-account-verification', 'UsersController@updateAccountVerification');
            Route::post('/delete-user', 'UsersController@deleteUser');
            Route::post('/approve-verified-account-request', 'UsersController@updateVerifiedAccountRequest');

            //  Subscription Plans
            Route::post('/add-edit-subscription-plan', 'SubscriptionController@addEditSubscriptionPlan');
            Route::post('/get-all-subscriptions-plan-list', 'SubscriptionController@getALLSubscriptionPlans');
            Route::post('/add-edit-subscription-plan-details', 'SubscriptionController@addEditSubscriptionPlanDetails');
            Route::post('/add-edit-subscription-plan-description', 'SubscriptionController@addEditSubscriptionPlanDescription');
            Route::get('/delete-subscription-plan/{id}', 'SubscriptionController@deleteSubscriptionPlans');
            Route::get('/delete-subscription-plan-details/{id}', 'SubscriptionController@deleteSubscriptionPlanDetail');
            Route::get('/delete-subscription-plan-description/{id}', 'SubscriptionController@deleteSubscriptionPlanDescription');
            Route::post('/get-subscription-plan', 'SubscriptionController@getDetails');
            Route::post('/get-subscription-plan-detail', 'SubscriptionController@getSubscriptionPlanDetail');
            Route::post('/get-subscription-plan-descriptions', 'SubscriptionController@getSubscriptionPlanDescriptionsList');
            Route::post('/get-subscription-plan-description', 'SubscriptionController@getSubscriptionPlanDescription');
            Route::post('/list-subscription-plans', 'SubscriptionController@getSubscriptionPlans');
            Route::post('/list-subscription-plans-details', 'SubscriptionController@getSubscriptionPlanDetails');
            Route::post('/get-verified-account-request-list', 'UsersController@getVerifiedAccountRequestList');
            Route::post('/list-subscriptions', 'SubscriptionController@getSubscriptions');

            Route::post('/add-edit-event', 'EventController@addEditEvent');
            Route::post('/list-events', 'EventController@getList');
            Route::post('/get-event', 'EventController@getDetails');
            Route::post('/delete-event', 'EventController@deleteEvent');

            Route::post('/add-edit-news', 'NewsController@addEditNews');
            Route::post('/list-news', 'NewsController@getList');
            Route::post('/get-news', 'NewsController@getDetails');
            Route::post('/delete-news', 'NewsController@deleteNews');

            Route::post('/add-edit-announcement', 'AnnouncementController@addEditAnnouncement');
            Route::post('/list-announcements', 'AnnouncementController@getList');
            Route::post('/get-announcement', 'AnnouncementController@getDetails');
            Route::post('/delete-announcement', 'AnnouncementController@deleteAnnouncement');

        });
    });
});
