<?php

namespace App\AdminApi;

use App\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Exceptions\JWTException;


class AdminApiController extends Controller
{

    public static $DEFAULT_PER_PAGE = 10; //for pagination
    public static $DEFAULT_ORDERBY_COLUMN = 'updated_at'; //sorting
    public static $DEFAULT_ORDERBY_DIRECTION = 'desc'; //sorting direction
    public static $SUCCESS = 1;
    public static $FAIL = 0;
    public static $UNAUTHORIZED_USER = Response::HTTP_UNAUTHORIZED;
    public static $VALIDATION_FAILED_HTTP_CODE = Response::HTTP_BAD_REQUEST;
    public static $HTTP_NOT_ACCEPTABLE = Response::HTTP_NOT_ACCEPTABLE;
    //Status
    public static $INACTIVE = 0;
    public static $ACTIVE = 1;
    public static $DELETE = 2;

    public static $YES = 1;
    public static $NO = 0;
    public static $ACCOUNT_VERIFICATION = 1;

    public function successFailResponse($data, $message, $status)
    {
        return [
            'data' => $data,
            'meta' => [
                'code' => $status,
                'message' => __($message)
            ]
        ];
    }

    public static function metaResponse($message, $status)
    {
        return [
            'code' => $status,
            'message' => __($message)
        ];
    }

    public function unAuthenticatedResponse($message, $status_code = Response::HTTP_UNAUTHORIZED)
    {
        return response()->json([
            'message' => __($message),
            'code' => $status_code
        ], $status_code);
    }

    public static function validationResponse($validator)
    {
        return response()->json(["message" => __($validator->errors()->first()), "code" => 400], 400);
    }

    /**
     * @param $api
     * @throws ValidationException
     */
    public function validateRequest($api)
    {
        $version = \Request::segment(2);
        $module = \Request::segment(3);
        $rules = Config::get($module . "_validations.{$api}.{$version}.rules");

        if (!$rules) {
            $rules = Config::get("admin_validations.{$api}.v1.rules");
        }

        $messages = Config::get($module . "_validations.{$api}.{$version}.messages");
        if (!$messages) {
            $messages = Config::get("admin_validations.{$api}.v1.messages");
        }

        if ($rules && $messages) {
            $messages = collect($messages)->map(function ($message) {
                return __($message);
            })->toArray();

            $payload = \Request::only(array_keys($rules));
            $validator = Validator::make($payload, $rules, $messages);
            if ($validator->fails()) {
                throw new \Illuminate\Validation\ValidationException($validator, static::$VALIDATION_FAILED_HTTP_CODE);
                $this->error($validator->errors()->first(), 400);
            }
        }
    }


    public function generateAccessToken($user)
    {
        $user = Admin::whereEmail($user['email'])->orderBy('id', 'desc')->first();
        $token = $user->createToken('Admin')->accessToken;
        if ($token) return $token;
    }

    public static function generateOtpAdmin(&$codes = [])
    {
        $code = mt_rand(100000, 999999);
        $admin = Admin::whereOtp($code)->first();
        if ($admin && in_array($code, $codes)) {
            return AdminApiController::generateOtpAdmin($codes[] = $code);
        } else {
            return $code;
        }
    }

    public static function generateTokenExpiryDate($days)
    {
        $current_timestamp = Carbon::now()->timestamp;
        $days_to_seconds = $days * 24 * 3600;
        $expiry_date = $current_timestamp + $days_to_seconds;
        return Carbon::parse($expiry_date)->toDateTimeString();
    }

    public static function listDateFormat($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }

    public static function removeFile($filePath, $modifications = [])
    {
        $storagePath = storage_path($filePath);
        if (!is_dir($storagePath) && file_exists($storagePath)) {
            unlink($storagePath);
        }
        return true;
    }

    public static function trueFalseConversion($value)
    {
        return $value == 1;
    }

    public static function carbonDate($date = null, $format = null)
    {
        if ($date) {
            $date = Carbon::parse($date)->format($format ?? 'Y-m-d H:i:s');
        } else {
            $date = Carbon::now()->format($format ?? 'Y-m-d H:i:s');
        }
        return $date;
    }

    public static function removeImage($filePath, $modifications = [])
    {
        $storagePath = storage_path('images/' . $filePath);
        if (!is_dir($storagePath) && file_exists($storagePath)) {
            unlink($storagePath);
        }
        return true;
    }
}
