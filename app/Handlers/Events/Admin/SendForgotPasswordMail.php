<?php

namespace App\Handlers\Events\Admin;

use App\Events\Admin\ForgotPasswordMail;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Config;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendForgotPasswordMail extends Mailable implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ForgotPasswordMail  $event
     * @return void
     */
    public function handle(ForgotPasswordMail $event)
    {
        if(Config::get('mail.send_mail')) {
            Mail::send('mails.admin.forgot-password', ['otp' => $event->otp,'first_name' => $event->first_name], function(Message $message) use ($event) {
                $message->to($event->email);
                $subject = 'Reset Your Password';
                $message->subject($subject);
            });
        }
    }
}
