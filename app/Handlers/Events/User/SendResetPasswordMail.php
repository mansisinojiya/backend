<?php

namespace App\Handlers\Events\User;

use App\Events\User\UserResetPasswordMail;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Config;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendResetPasswordMail extends Mailable implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserResetPasswordMail $event
     * @return void
     */
    public function handle(UserResetPasswordMail $event)
    {
        if (Config::get('mail.send_mail')) {
            Mail::send('mails.user.reset-password', ['password' => $event->password, 'name' => $event->name],
                function (Message $message) use ($event) {
                    $message->to($event->email);
                    $subject = 'Password Reset Successfully';
                    $message->subject($subject);
                });
        }
    }
}
