<?php

namespace App\Handlers\Events\User;

use App\Events\User\UserForgotPasswordMail;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Config;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendForgotPasswordLinkMail extends Mailable implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserForgotPasswordMail $event
     * @return void
     */
    public function handle(UserForgotPasswordMail $event)
    {
        if (Config::get('mail.send_mail')) {
            Mail::send('mails.user.forgot-password', ['otp' => $event->otp, 'name' => $event->name],
                function (Message $message) use ($event) {
                    $message->to($event->email);
                    $subject = 'Reset Your Password';
                    $message->subject($subject);
                });
        }
    }
}
