<?php

namespace App\Handlers\Events\User;

use App\Events\User\AccountVerificationMail;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Config;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendAccountVerificationMail extends Mailable implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param AccountVerificationMail $event
     * @return void
     */
    public function handle(AccountVerificationMail $event)
    {
        if (Config::get('mail.send_mail')) {
            Mail::send('mails.user.account-verification', ['link' => $event->link, 'name' => $event->email],
                function (Message $message) use ($event) {
                    $message->to($event->email);
                    $subject = 'Verify Your Account';
                    $message->subject($subject);
                });
        }
    }
}
