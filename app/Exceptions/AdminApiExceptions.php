<?php

namespace App\Exceptions;

use App\AdminApi\AdminApiController;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

trait AdminApiExceptions {

    public function adminApiException($request, $exception){
        if ($exception instanceof ModelNotFoundException){
            return response()->json([
                'message' => \Lang::get('admin_api.model_not_found'),
                'code' => 400
            ],AdminApiController::$VALIDATION_FAILED_HTTP_CODE);
        }

        if ($exception instanceof NotFoundHttpException){
            return response()->json([
                'message' => \Lang::get('admin_api.url_not_found'),
                'code' => 400
            ],AdminApiController::$VALIDATION_FAILED_HTTP_CODE);
        }

        if ($exception instanceof \Illuminate\Validation\ValidationException){
            return response()->json([
                'message' => $exception->validator->errors()->first(),
                'code' => 400
            ],AdminApiController::$VALIDATION_FAILED_HTTP_CODE);
        }

        if ($exception instanceof AuthenticationException){
            return response()->json([
                'message' => \Lang::get('admin_api.unauthorized_user'),
                'code' => 401
            ],AdminApiController::$UNAUTHORIZED_USER);
        }

        if ($exception instanceof MethodNotAllowedHttpException){
            return response()->json([
                'message' => \Lang::get('admin_api.method_not_found'),
                'code' => 400
            ],AdminApiController::$VALIDATION_FAILED_HTTP_CODE);
        }

        return parent::render($request, $exception);

    }

}