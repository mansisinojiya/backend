<?php

namespace App\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserDetailChangeReq;
use App\Models\VerifyToken;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;


class ApiController extends Controller
{

    public static $DEFAULT_PER_PAGE = 10; //for pagination
    public static $DEFAULT_ORDERBY_COLUMN = 'updated_at'; //sorting
    public static $DEFAULT_ORDERBY_DIRECTION = 'desc'; //sorting direction
    public static $SUCCESS = 1;
    public static $FAIL = 0;
    public static $UNAUTHORIZED_USER = Response::HTTP_UNAUTHORIZED;
    public static $VALIDATION_FAILED_HTTP_CODE = Response::HTTP_BAD_REQUEST;
    public static $HTTP_NOT_ACCEPTABLE = Response::HTTP_NOT_ACCEPTABLE;

    //Status
    public static $INACTIVE = 0;
    public static $ACTIVE = 1;
    public static $DELETE = 2;
    public static $YES = 1;
    public static $NO = 0;
    public static $ACCOUNT_VERIFICATION = 1;
    public static $ACCOUNT_VERIFICATION_NO = 2;
    public static $FORGOT_PASSWORD = 2;
    public static $FRIEND_REQUEST = 1;
    public static $FRIEND_ACCEPTED = 1;
    public static $FRIEND_DECLINED = 3;
    public static $FRIEND_REQUEST_SENT = 1;
    public static $FRIEND_REQUEST_ACCEPTED = 2;
    public static $FRIEND_REQUEST_DECLINED = 3;
    public static $EVENT_ADDED = 4;
    public static $NEWS_ADDED = 5;
    public static $ANNOUNCEMENT_ADDED = 6;
    public static $VOTE_UP = 7;
    public static $FRIEND_REQUEST_ACCEPTED_BY_USER = 8;
    public static $WEEKLY_PLAYER_PLAN = 9;
    public static $WEEKLY_PLAYER_PLAN_REQUEST = 10;
    public static $WEEKLY_PLAYER_PLAN_REQUEST_ACCEPTED = 11;

    public function successFailResponse($data, $message, $status)
    {
        return [
            'data' => $data,
            'meta' => [
                'code' => $status,
                'message' => __($message)
            ]
        ];
    }

    public static function metaResponse($message, $status)
    {
        return [
            'code' => $status,
            'message' => __($message)
        ];
    }

    public static function unAuthenticatedResponse($message, $status_code = Response::HTTP_UNAUTHORIZED)
    {
        return response()->json([
            'message' => __($message),
            'code' => $status_code
        ], $status_code);
    }

    public static function validationResponse($validator)
    {
        return response()->json(["message" => __($validator->errors()->first()), "code" => 400], 400);
    }

    /**
     * @param $api
     * @throws ValidationException
     */
    public function validateRequest($api)
    {
        $version = \Request::segment(2);
        $rules = Config::get("api_validations.{$api}.{$version}.rules");

        if (!$rules) {
            $rules = Config::get("api_validations.{$api}.v1.rules");
        }

        $messages = Config::get("api_validations.{$api}.{$version}.messages");
        if (!$messages) {
            $messages = Config::get("api_validations.{$api}.v1.messages");
        }

        if ($rules && $messages) {
            $messages = collect($messages)->map(function ($message) {
                return __($message);
            })->toArray();

            $payload = \Request::only(array_keys($rules));
            $validator = Validator::make($payload, $rules, $messages);
            if ($validator->fails()) {
                throw new \Illuminate\Validation\ValidationException($validator, static::$VALIDATION_FAILED_HTTP_CODE);
                $this->error($validator->errors()->first(), 400);
            }
        }
    }

    public function generateAccessToken($user)
    {
        $user = User::whereEmail($user->email)->orderBy('id', 'desc')->first();
        $token = $user->createToken('User')->accessToken;
        if ($token) return $token;
    }

    public static function generateTokenExpiryDate($days)
    {
        $current_timestamp = Carbon::now()->timestamp;
        $days_to_seconds = $days * 24 * 3600;
        $expiry_date = $current_timestamp + $days_to_seconds;
        return Carbon::parse($expiry_date)->toDateTimeString();
    }

    public static function generateVerificationToken($type, &$codes = [])
    {
        $code = md5(uniqid(rand(), true));
        if ($type == 'verification') {
            $verify_token = VerifyToken::whereToken($code)->first();
        } else $verify_token = VerifyToken::whereToken($code)->first();
        if ($verify_token || in_array($code, $codes)) {
            return ApiController::generateVerificationToken($codes[] = $code);
        } else {
            return $code;
        }
    }

    public static function removeFile($filePath, $modifications = [])
    {
        $storagePath = storage_path($filePath);
        if (!is_dir($storagePath) && file_exists($storagePath)) {
            unlink($storagePath);
        }
        return true;
    }

    public static function carbonDate($date = null, $format = null)
    {
        if ($date) {
            $date = Carbon::parse($date)->format($format ?? 'Y-m-d H:i:s');
        } else {
            $date = Carbon::now()->format($format ?? 'Y-m-d H:i:s');
        }
        return $date;
    }

    public static function generateOtpUser(&$codes = [])
    {
        $code = mt_rand(100000, 999999);
        $user = User::whereOtp($code)->first();
        if ($user && in_array($code, $codes)) {
            return ApiController::generateOtpUser($codes[] = $code);
        } else {
            return $code;
        }
    }

    public static function removeImage($filePath, $modifications = [])
    {
        $storagePath = storage_path('images/' . $filePath);
        if (!is_dir($storagePath) && file_exists($storagePath)) {
            unlink($storagePath);
        }
        return true;
    }
}
