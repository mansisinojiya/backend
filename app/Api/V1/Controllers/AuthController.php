<?php

namespace App\Api\V1\Controllers;

use App\Api\ApiController;
use App\Api\V1\Resources\Auth\UserAuthResource;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends ApiController
{
    public function register(Request $request)
    {
        $this->validateRequest('register');
        $checkEmail = User::whereEmail(strtolower($request->get('email')))->first();
        if ($checkEmail)
            return ApiController::successFailResponse(null, 'api.email_already_register', ApiController::$FAIL);
        try {
            DB::beginTransaction();
            $user = new User();
            $user->lastname = $request->get('lastname');
            $user->firstname = $request->get('firstname');
            $user->email = $request->get('email');
            $user->city_id = $request->get('city_id');
            $user->hobby_id = $request->get('hobby_id');
            $user->gender = $request->get('gender');
            $user->password = \Hash::make($request->get('password'));
            $user->pincode = $request->get('pincode');
            $user->mobile = $request->get('mobile');
            $user->status = 0;
            $user->save();
            DB::commit();
            return ApiController::successFailResponse(null, 'api.success', ApiController::$SUCCESS);
        } catch (Exception $exception) {
            DB::rollBack();
            return ApiController::successFailResponse(null, 'api.something_went_wrong', ApiController::$FAIL);
        }
    }

    public function login(Request $request)
    {
        $this->validateRequest('login');
        $requestParam = $request->all();
        $user = User::whereEmail(strtolower($requestParam['email']))->orderBy('id', 'desc')->first();
        if (!$user)
            return ApiController::successFailResponse(null, 'api.invalid_user', ApiController::$FAIL);
        if ($user && Hash::check($requestParam['password'], $user->password)) {
            $token = $this->generateAccessToken($user);
            return UserAuthResource::make($user)->additional(['meta' => ['message' => __('api.loginSuccess'), 'code' => ApiController::$SUCCESS, 'token' => $token,]]);
        } else {
            return ApiController::unAuthenticatedResponse('api.wrongCredentials');
        }
    }

    public function changePassword(Request $request)
    {
        $this->validateRequest('changePassword');
        $user = User::whereId(Auth::user()->id)->first();
        if (Hash::check($request->get('old_password'), $user->password)) {
            $user->password = \Hash::make($request->get('password'));
            $user->save();
            return ApiController::successFailResponse(null, 'api.password_change_success', ApiController::$SUCCESS);
        } else return ApiController::successFailResponse(null, 'api.invalidOldPassword', ApiController::$FAIL);
    }

    public function getDetails(Request $request)
    {
        $user = Auth::user();
        return UserAuthResource::make($user)->additional(['meta' => ['message' => __('api.success'), 'code' => ApiController::$SUCCESS,]]);
    }
}
