<?php
Route::group(['middleware' => ['api-logger'], 'prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($route) {

    Route::group(['middleware' => 'api-auth'], function () {
        Route::post('/register', 'AuthController@register');
        Route::get('/verify-account', 'AuthController@verifyAccount');
        Route::post('/login', 'AuthController@login');
        Route::post('/forgot-password', 'AuthController@forgotPassword');
        Route::post('/reset-password', 'AuthController@resetPassword');

        Route::group(['middleware' => ['auth:api']], function () {

            Route::post('/change-password', 'AuthController@changePassword');
            Route::post('/edit-profile', 'AuthController@editProfile');
            Route::get('/get-details', 'AuthController@getDetails');
            Route::post('/get-user-detail', 'AuthController@getUserDetail');
            Route::post('/user-availibility', 'AuthController@userAvailabilty');
            Route::post('/delete-account', 'AuthController@deleteAccount');
            Route::post('/deactivate-account', 'AuthController@deactivateAccount');
            Route::post('/pause-account', 'AuthController@pauseAccount');
            Route::post('/request-verified-account', 'AuthController@requestVerifiedAccount');

            Route::post('/add-edit-user-experience', 'UserExperienceController@addEditUserExperience');
            Route::post('/get-user-experience', 'UserExperienceController@getUserExperience');
            Route::post('/get-all-user-experience', 'UserExperienceController@getAllUserExperience');
            Route::get('/delete-user-experience/{id}', 'UserExperienceController@deleteUserExperience');
            Route::post('/get-all-positions', 'PositionController@getAllPositions');
            Route::post('/get-all-sub-positions', 'SubPositionController@getAllSubPositions');
            Route::post('/get-all-users', 'AuthController@getAllUsersByRole');


            Route::post('/add-user-to-connection-list', 'AuthController@addUserToConnectionList');
            Route::post('/get-all-connection-list', 'AuthController@getAllConnectionList');


            Route::post('/compare-profile', 'AuthController@compareProfile');
            Route::post('/set-like-dislikes', 'LikeDislikesController@setLikeDislikes');
            Route::post('/get-like-dislikes', 'LikeDislikesController@getLikeDislikes');


            Route::post('/subscribe', 'SubscriptionController@Subscribe');
            Route::post('/cancel-subscription', 'SubscriptionController@cancelSubscription');
            Route::post('/subscription-details', 'SubscriptionController@getSubscriptionDetails');
            Route::post('/subscription-plans', 'SubscriptionController@getALLSubscriptionPlans');
            Route::post('/subscribed-players-for-blue-plan', 'SubscriptionController@subscribedPlayers');
            Route::post('/accept-subscribed-request', 'SubscriptionController@acceptSubscribeRequest');


            Route::post('/send-friend-request', 'FriendRequestController@sendFriendRequest');
            Route::post('/get-friend-request', 'FriendRequestController@getFriendRequests');
            Route::post('/response-friend-request', 'FriendRequestController@responseFriendRequest');
            Route::post('/get-all-friend-list', 'FriendRequestController@getFriendList');


            Route::post('/add-user-to-recommendation-list', 'RecommendationController@addUserToRecommendation');
            Route::post('/add-user-to-reject-recommendation', 'RecommendationController@addUserToRejectedRecommadation');
            Route::post('/get-all-recommendation-list', 'RecommendationController@getAllRecommendation');

            Route::post('/get-all-notifications', 'NotificationController@getAllNotifications');
            Route::post('/read-notification', 'NotificationController@readNotification');

            Route::post('/add-edit-event', 'EventController@addEditEvent');
            Route::post('/list-events', 'EventController@getList');
            Route::post('/get-event', 'EventController@getDetails');
            Route::post('/delete-event', 'EventController@deleteEvent');

            Route::post('/add-edit-news', 'NewsController@addEditNews');
            Route::post('/list-news', 'NewsController@getList');
            Route::post('/get-news', 'NewsController@getDetails');
            Route::post('/delete-news', 'NewsController@deleteNews');

            Route::post('/add-edit-announcement', 'AnnouncementController@addEditAnnouncement');
            Route::post('/list-announcements', 'AnnouncementController@getList');
            Route::post('/get-announcement', 'AnnouncementController@getDetails');
            Route::post('/delete-announcement', 'AnnouncementController@deleteAnnouncement');

            Route::post('/add-edit-media', 'MediaController@addEditMedia');
            Route::post('/list-media', 'MediaController@getList');
            Route::post('/get-media', 'MediaController@getDetails');
            Route::post('/delete-media', 'MediaController@deleteMedia');
            Route::post('/player-voting', 'MediaController@PlayerVoting');
            Route::post('/list-media-by-official-name', 'MediaController@getMediaByOfficialName');
        });
    });
});
