<?php

namespace App\Api\V1\Resources\Auth;


use App\Api\ApiController;
use App\Models\Country;
use App\Models\Language;
use App\Models\Position;
use App\Models\SubPosition;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;


class UserAuthResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'mobile' => $this->mobile,
            'city_id' => $this->city_id,
            'pincode' => $this->pincode ? $this->pincode : '',
            'created_at' => ApiController::carbonDate($this->created_at),
            'updated_at' => ApiController::carbonDate($this->updated_at),
        ];
    }
}
