<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\VerifyToken;
use Carbon\Carbon;
use Spatie\Glide\GlideImage;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class VerifyAccountController
{
    public static $DEFAULT_PER_PAGE = 10; //for pagination
    public static $DEFAULT_ORDERBY_COLUMN = 'updated_at'; //sorting
    public static $DEFAULT_ORDERBY_DIRECTION = 'desc'; //sorting direction
    public static $SUCCESS = 1;
    public static $FAIL = 0;
    public static $UNAUTHORIZED_USER = Response::HTTP_UNAUTHORIZED;
    public static $VALIDATION_FAILED_HTTP_CODE = Response::HTTP_BAD_REQUEST;
    public static $HTTP_NOT_ACCEPTABLE = Response::HTTP_NOT_ACCEPTABLE;
    //Status
    public static $INACTIVE = 0;
    public static $ACTIVE = 1;
    public static $DELETE = 2;

    public static $YES = 1;
    public static $NO = 0;
    public static $ACCOUNT_VERIFICATION = 1;
    public function successFailResponse($data, $message, $status)
    {
        return [
            'data' => $data,
            'meta' => [
                'code' => $status,
                'message' => __($message)
            ]
        ];
    }

    public function verifyAccount(Request $request)
    {
        $verify_token = VerifyToken::whereToken($request->token)->whereType(self::$ACCOUNT_VERIFICATION)->first();
        if ($verify_token) {
            if (Carbon::parse($verify_token->expired_date)->timestamp > Carbon::now()->timestamp) {
                $user = User::whereId($verify_token->user_id)->first();
                if (!$user) return 'Invalid user.';
                $user->is_verify = self::$YES;
                $user->save();
                $verify_token->forceDelete();
                return 'Account has been verified successfully.';
            } else return 'Your token is expired. Please try again.';
        } else return 'Please enter valid Token.';
    }
}
