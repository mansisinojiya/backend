<?php

namespace App\Http\Controllers;

use Spatie\Glide\GlideImage;
use Illuminate\Http\Request;

class HomeController
{
    public function getHome(){
        return view('welcome');
    }
}
