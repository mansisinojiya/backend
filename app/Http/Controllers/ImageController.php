<?php

namespace App\Http\Controllers;

use Spatie\Glide\GlideImage;
use Illuminate\Http\Request;

class ImageController
{
    public function getImage(Request $request)
    {
        ## Check folder exists or not
        $folder = $request->folder;
        $uid = $request->id?$request->id:$request->postId;
        $img = $request->name;
        $i = explode('.', $img);

        $imagePath = $folder . '/' . $img;
        if ($uid) {
            $imagePath = $folder . '/' . $uid;
            $imagePath = $imagePath . '/' . $img;
        }

        if (!$img) {
            return "";
        } else if ($i[count($i) - 1] == 'jpg' || $i[count($i) - 1] == 'png' || $i[count($i) - 1] == 'jpeg') {
            $imageFolder = storage_path('images/cache/' . $folder);

            if (!is_dir($imageFolder)) {
                mkdir($imageFolder, 0777, true);
                chmod($imageFolder, 0777);
            }

            $pathToImage = storage_path('images/' . $imagePath);
            $width = \Request::get('w');
            $height = \Request::get('h');
            $reqFit = \Request::get('fit');

            $fit = "";
            if ($reqFit) {
                if (in_array($reqFit, ['contain', 'max', 'fill', 'stretch', 'crop'])) {
                    $fit = $reqFit;
                }
            }

            $tmpImageArr = (explode('.', $img));
            $ext = end($tmpImageArr);
            $image_name = basename($img, '.' . $ext);
            $cacheImage = $image_name . '-' . (($width) ? $width : 0) . 'x' . (($height) ? $height : 0) . $fit;
            $cacheImage = $cacheImage . '.' . $ext;
            $newImage = 'images/cache/' . $folder . '/' . $cacheImage;

            if (!\Storage::exists($newImage)) {
                GlideImage::create($pathToImage)
                    ->modify(['w' => $width, 'h' => $height, 'fit' => $fit])
                    ->save(storage_path($newImage));
            }
            return response()->file(storage_path($newImage));
        } else {
            return response()->file(storage_path('images/' . $imagePath));
        }
    }
}
