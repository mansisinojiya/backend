<?php

namespace App\Http\Middleware;

use App\AdminApi\AdminApiController;
use Closure;

class AdminApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin = \Auth::user();
        if ($admin) {
            if ($admin) {
                return $next($request);
            } else {
                return response()->json([
                    'message' => \Lang::get('admin_api.unauthorized_user'),
                    'code' => 401
                ], AdminApiController::$UNAUTHORIZED_USER);
            }
        }
        return $next($request);
    }
}
