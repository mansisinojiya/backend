<?php

return [
    'register' => [
        'v1' => [
            'rules' => [
                'email' => 'required|email',
                'password' => 'required',
                'firstname' => 'required',
                'lastname' => 'required',
                'city_id' => 'required',
                'hobby_id' => 'required',
                'gender' => 'required',
                'pincode' => 'required',
                'mobile' => 'required',
            ],
            'messages' => [
                'email.required' => 'api.emailRequired',
                'email.email' => 'api.emailNotValid',
                'password.required' => 'api.passwordRequired',
                'mobile.required' => 'api.please_provide_mobile',
                'firstname.required' => 'api.please_provide_firstname',
                'lastname.required' => 'api.please_provide_lastname',
                'city_id.required' => 'api.please_provide_city_id',
                'hobby_id.required' => 'api.please_provide_hobby_id',
                'gender.required' => 'api.please_provide_gender',
                'pincode.required' => 'api.please_provide_pincode',
            ],
        ],
    ],
    'verifyToken' => [
        'v1' => [
            'rules' => [
                'token' => 'required',
            ],
            'messages' => [
                'token.required' => 'api.invalid_token',
            ]
        ],
    ],
    'login' => [
        'v1' => [
            'rules' => [
                'email' => 'required|email',
                'password' => 'required',
            ],
            'messages' => [
                'email.required' => 'api.emailRequired',
                'email.email' => 'api.emailNotValid',
                'password.required' => 'api.passwordRequired'
            ],
        ],
    ],
    'forgot-password' => [
        'v1' => [
            'rules' => [
                'email' => 'required|email',
                'password' => 'required',
            ],
            'messages' => [
                'email.required' => 'api.emailRequired',
                'email.email' => 'api.emailNotValid',
                'password.required' => 'api.passwordRequired',
            ]
        ]
    ],
    'forgotPassword' => [
        'v1' => [
            'rules' => [
                'email' => 'required|email',
            ],
            'messages' => [
                'email.required' => 'api.emailRequired',
                'email.email' => 'api.emailNotValid',
            ]
        ]
    ],
    'resetPassword' => [
        'v1' => [
            'rules' => [
                'otp' => 'required',
                'password' => 'required',
            ],
            'messages' => [
                'otp.required' => 'api.otp_required',
                'password.required' => 'api.passwordRequired'
            ]
        ],
    ],
    'changePassword' => [
        'v1' => [
            'rules' => [
                'old_password' => 'required',
                'password' => 'required',
            ],
            'messages' => [
                'old_password.required' => 'api.old_password_cant_be_blank',
                'password.required' => 'api.newPasswordRequired'
            ],
        ],
    ],
    'editProfile' => [
        'v1' => [
            'rules' => [
                'name' => 'required|max:150',
            ],
            'messages' => [
                'name.required' => 'api.name_cant_be_blank',
                'name.max' => 'api.name_max_limit',
            ],
        ],
    ],

    'login-fb' => [
        'v1' => [
            'rules' => [
                'email' => 'required|email',
                'facebook_id' => 'required',
                'device_id' => 'required',
                'device_type' => 'required',
                'firebase_id' => 'required',
                'firebase_token' => 'required',
                'type' => 'required',
            ],
            'messages' => [
                'email.required' => 'api.please_enter_registered_email_address',
                'email.email' => 'api.please_enter_valid_email_address',
                'facebook_id.required' => 'api.please_enter_facebook_id',
                'device_id.required' => 'api.please_provide_device_id',
                'device_type.required' => 'api.please_provide_device_type',
                'firebase_id.required' => 'api.please_provide_firebase_id',
                'firebase_token.required' => 'api.please_provide_firebase_token',
                'type.required' => 'api.type_cant_blank',
            ],
        ],
    ],

    'login-insta' => [
        'v1' => [
            'rules' => [
                'email' => 'required|email',
                'instagram_id' => 'required',
                'device_id' => 'required',
                'device_type' => 'required',
                'firebase_id' => 'required',
                'firebase_token' => 'required',
                'type' => 'required',
            ],
            'messages' => [
                'email.required' => 'api.please_enter_registered_email_address',
                'email.email' => 'api.please_enter_valid_email_address',
                'instagram_id.required' => 'api.please_enter_instagram_id',
                'device_id.required' => 'api.please_provide_device_id',
                'device_type.required' => 'api.please_provide_device_type',
                'firebase_id.required' => 'api.please_provide_firebase_id',
                'firebase_token.required' => 'api.please_provide_firebase_token',
                'type.required' => 'api.type_cant_blank',
            ],
        ],
    ],

    'login-linkedin' => [
        'v1' => [
            'rules' => [
                'email' => 'required|email',
                'linkedin_id' => 'required',
                'device_id' => 'required',
                'device_type' => 'required',
                'firebase_id' => 'required',
                'firebase_token' => 'required',
                'type' => 'required',
            ],
            'messages' => [
                'email.required' => 'api.please_enter_registered_email_address',
                'email.email' => 'api.please_enter_valid_email_address',
                'linkedin_id.required' => 'api.please_enter_linkedin_id',
                'device_id.required' => 'api.please_provide_device_id',
                'device_type.required' => 'api.please_provide_device_type',
                'firebase_id.required' => 'api.please_provide_firebase_id',
                'firebase_token.required' => 'api.please_provide_firebase_token',
                'type.required' => 'api.type_cant_blank',
            ],
        ],
    ],

    'login-google' => [
        'v1' => [
            'rules' => [
                'email' => 'required|email',
                'google_id' => 'required',
                'device_id' => 'required',
                'device_type' => 'required',
                'firebase_id' => 'required',
                'firebase_token' => 'required',
                'type' => 'required',
            ],
            'messages' => [
                'email.required' => 'api.please_enter_registered_email_address',
                'email.email' => 'api.please_enter_valid_email_address',
                'google_id.required' => 'api.please_enter_google_id',
                'device_id.required' => 'api.please_provide_device_id',
                'device_type.required' => 'api.please_provide_device_type',
                'firebase_id.required' => 'api.please_provide_firebase_id',
                'firebase_token.required' => 'api.please_provide_firebase_token',
                'type.required' => 'api.type_cant_blank',
            ],
        ],
    ],

    'addEditUserExperience' => [
        'v1' => [
            'rules' => [
                'position' => 'required',
                'club_name' => 'required',
                'location' => 'required',
                'description' => 'required',
                'media' => 'required',
                'from_date' => 'required',
                'to_date' => 'required',
            ],
            'messages' => [
                'position.required' => 'api.please_enter_position',
                'club_name.required' => 'api.please_enter_club_name',
                'location.required' => 'api.please_enter_location',
                'description.required' => 'api.please_enter_description',
                'media.required' => 'api.please_enter_media',
                'from_date.required' => 'api.please_enter_from_date',
                'to_date.required' => 'api.please_enter_to_date',
            ],
        ],
    ],
    'setLikeDislikes' => [
        'v1' => [
            'rules' => [
                'user_id' => 'required',
                'is_like' => 'required',
            ],
            'messages' => [
                'user_id.required' => 'api.user_id_is_required',
                'is_like.required' => 'api.is_likes_is_required',
            ],
        ],
    ],
    'sendFriendRequest' => [
        'v1' => [
            'rules' => [
                'to_user_id' => 'required',
            ],
            'messages' => [
                'to_user_id.required' => 'api.to_user_id_is_required',
            ],
        ],
    ],
    'acceptFriendRequest' => [
        'v1' => [
            'rules' => [
                'from_user_id' => 'required',
                'is_accepted' => 'required',
            ],
            'messages' => [
                'from_user_id.required' => 'api.from_user_id_is_required',
                'is_accepted.required' => 'api.is_accepted_is_required',
            ],
        ],
    ],
    'Subscription' => [
        'v1' => [
            'rules' => [
                'subscription_plan_id' => 'required',
                'subscription_plans_details_id' => 'required',
            ],
            'messages' => [
                'subscription_plan_id.required' => 'api.plan_id_required',
                'subscription_plans_details_id.required' => 'api.plan_detail_id_required',
            ],
        ],
    ],
    'subscribeByOther' => [
        'v1' => [
            'rules' => [
                'player_id' => 'required',
            ],
            'messages' => [
                'player_id.required' => 'api.player_id_required',
            ],
        ],
    ],
    'addEditEvent' => [
        'v1' => [
            'rules' => [
                'date' => 'required',
                'time' => 'required',
                'image' => 'required',
                'text' => 'required',
                'heading' => 'required',
            ],
            'messages' => [
                'date.required' => 'api.date_required',
                'time.required' => 'api.time_required',
                'image.required' => 'api.image_required',
                'text.required' => 'api.text_required',
                'heading.required' => 'api.heading_required',
            ],
        ],
    ],
    'addEditNews' => [
        'v1' => [
            'rules' => [
                'image' => 'required',
                'heading' => 'required',
            ],
            'messages' => [
                'image.required' => 'api.image_required',
                'heading.required' => 'api.heading_required',
            ],
        ],
    ],
    'addEditAnnouncement' => [
        'v1' => [
            'rules' => [
                'image' => 'required',
                'text' => 'required',
            ],
            'messages' => [
                'image.required' => 'api.image_required',
                'text.required' => 'api.text_required',
            ],
        ],
    ],
    'addEditMedia' => [
        'v1' => [
            'rules' => [
                'video' => 'required',
            ],
            'messages' => [
                'video.required' => 'api.video_required',
            ],
        ],
    ],
];
