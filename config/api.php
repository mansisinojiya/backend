<?php

return [
    'api_log' => true,
    'send_notification' => true,
    'send_mail' => true,
    'api_url' => 'http://122.169.109.79:4280/fbbackend/',
    'USER_PANEL_URL' => 'http://localhost:4200',
    'app_url' => 'http://localhost/fbbackend',
];
