<?php

return [
    'gcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'My_ApiKey',
    ],
    'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAAI46vNBU:APA91bG2HG1VgfbPK-OqIr4kRnzw1bwsBwXMORSvHnSljQWTtQwXp1xvxmSl4k7U_IcKJtlYAp-Or5ebq2IOI0Kvdooe79hAPlLjoOkG1rha6rugbwD1Tj8i9FhvYBw9gzpw_ydwPgc_',
    ],
    'apn' => [
        'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
        'passPhrase' => '1234', //Optional
        'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
        'dry_run' => true
    ],

    'send' => true, // true or false;

];
