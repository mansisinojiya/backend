<?php
return [
    'app_name' => 'Practical',
    'prefix' => 'Practical',
    'version' => '1.0.0',
    'default_per_page' => 10,
    'path' => 'admin'
];
