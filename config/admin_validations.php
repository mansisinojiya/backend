<?php

return [
    'login' => [
        'v1' => [
            'rules' => [
                'email' => 'required|email',
                'password' => 'required',
            ],
            'messages' => [
                'email.required' => 'admin_api.emailRequired',
                'email.email' => 'admin_api.emailNotValid',
                'password.required' => 'admin_api.passwordRequired'
            ],
        ],
    ],
    'forgotPassword' => [
        'v1' => [
            'rules' => [
                'email' => 'required|email',
            ],
            'messages' => [
                'email.required' => 'admin_api.emailRequired',
                'email.email' => 'admin_api.emailNotValid',
            ]
        ]
    ],
    'resetPassword' => [
        'v1' => [
            'rules' => [
                'otp' => 'required',
                'password' => 'required',
            ],
            'messages' => [
                'otp.required' => 'admin_api.otp_required',
                'password.required' => 'admin_api.passwordRequired'
            ]
        ],
    ],
    'changePassword' => [
        'v1' => [
            'rules' => [
                'old_password' => 'required',
                'password' => 'required',

            ],
            'messages' => [
                'old_password.required' => 'admin_api.old_password_cant_be_blank',
                'password.required' => 'admin_api.newPasswordRequired'
            ],
        ],
    ],
    'editProfile' => [
        'v1' => [
            'rules' => [
                'first_name' => 'required',
                'last_name' => 'required'
            ],
            'messages' => [
                'first_name.required' => 'admin_api.firstname_cant_be_blank',
                'last_name.required' => 'admin_api.lastname_cant_be_blank'
            ],
        ],
    ],

    'addEditCountry' => [
        'v1' => [
            'rules' => [
                'name' => 'required|max:150',
            ],
            'messages' => [
                'country_name.max' => 'admin_api.country_name_max_limit',
                'country_name.required' => 'admin_api.country_name_required',
            ],
        ],
    ],

    'addEditRoles' => [
        'v1' => [
            'rules' => [
                'name' => 'required|max:100',
            ],
            'messages' => [
                'role_name.max' => 'admin_api.role_name_max_limit',
                'role_name.required' => 'admin_api.role_name_required',
            ],
        ],
    ],

    'addEditLanguages' => [
        'v1' => [
            'rules' => [
                'name' => 'required|max:150',
            ],
            'messages' => [
                'language_name.max' => 'admin_api.language_name_max_limit',
                'language_name.required' => 'admin_api.language_name_required',
            ],
        ],
    ],

    'addEditPosition' => [
        'v1' => [
            'rules' => [
                'name' => 'required|max:150',
            ],
            'messages' => [
                'position_name.max' => 'admin_api.position_name_max_limit',
                'position_name.required' => 'admin_api.position_name_required',
            ],
        ],
    ],

    'addEditCity' => [
        'v1' => [
            'rules' => [
                'name' => 'required|max:150',
            ],
            'messages' => [
                'city_name.max' => 'admin_api.city_name_max_limit',
                'city_name.required' => 'admin_api.city_name_required',
            ],
        ],
    ],

    'addEditSubscriptionPlans' => [
        'v1' => [
            'rules' => [
                'plan_name' => 'required',
            ],
            'messages' => [
                'plan_name.required' => 'admin_api.plan_name_required',
            ],
        ],
    ],


    'addEditSubscriptionPlansDetails' => [
        'v1' => [
            'rules' => [
                'duration' => 'required',
                'price' => 'required',
                'time_period' => 'required',
                'subscription_plan_id' => 'required',
                'role_id' => 'required',
            ],
            'messages' => [
                'duration.required' => 'admin_api.duration_required',
                'price.required' => 'admin_api.price_required',
                'time_period.required' => 'admin_api.time_period_required',
                'subscription_plan_id.required' => 'admin_api.subscription_plan_id_required',
                'role_id.required' => 'admin_api.role_id_required',
            ],
        ],
    ],

    'addEditSubscriptionPlanDescription' => [
        'v1' => [
            'rules' => [
                'description' => 'required',
                'subscription_plan_id' => 'required',
            ],
            'messages' => [
                'description.required' => 'admin_api.description_required',
                'subscription_plan_id.required' => 'admin_api.subscription_plan_id_required',
            ],
        ],
    ],

    'addEditEvent' => [
        'v1' => [
            'rules' => [
                'date' => 'required',
                'time' => 'required',
                'image' => 'required',
                'text' => 'required',
                'heading' => 'required',
            ],
            'messages' => [
                'date.required' => 'api.date_required',
                'time.required' => 'api.time_required',
                'image.required' => 'api.image_required',
                'text.required' => 'api.text_required',
                'heading.required' => 'api.heading_required',
            ],
        ],
    ],
    'addEditNews' => [
        'v1' => [
            'rules' => [
                'image' => 'required',
                'heading' => 'required',
            ],
            'messages' => [
                'image.required' => 'api.image_required',
                'heading.required' => 'api.heading_required',
            ],
        ],
    ],
    'addEditAnnouncement' => [
        'v1' => [
            'rules' => [
                'image' => 'required',
                'text' => 'required',
            ],
            'messages' => [
                'image.required' => 'api.image_required',
                'text.required' => 'api.text_required',
            ],
        ],
    ],
];
