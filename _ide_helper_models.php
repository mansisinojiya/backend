<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Achievements
 *
 * @property int $id
 * @property int $user_id
 * @property string $achievement_year
 * @property string $team
 * @property string $achievement
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements newQuery()
 * @method static \Illuminate\Database\Query\Builder|Achievements onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements query()
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements whereAchievement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements whereAchievementYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements whereTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Achievements whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|Achievements withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Achievements withoutTrashed()
 * @mixin \Eloquent
 */
	class Achievements extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Admin
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string|null $mobile
 * @property int|null $otp
 * @property int $is_verify 1:Verify, 2:UnVerify
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin query()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereIsVerify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereOtp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Admin extends \Eloquent implements \Tymon\JWTAuth\Contracts\JWTSubject {}
}

namespace App\Models{
/**
 * App\Models\City
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|City newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|City newQuery()
 * @method static \Illuminate\Database\Query\Builder|City onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|City query()
 * @method static \Illuminate\Database\Eloquent\Builder|City whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|City withTrashed()
 * @method static \Illuminate\Database\Query\Builder|City withoutTrashed()
 * @mixin \Eloquent
 */
	class City extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ConnectionList
 *
 * @property int $id
 * @property int $user_id
 * @property int $connection_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ConnectionList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConnectionList newQuery()
 * @method static \Illuminate\Database\Query\Builder|ConnectionList onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ConnectionList query()
 * @method static \Illuminate\Database\Eloquent\Builder|ConnectionList whereConnectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConnectionList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConnectionList whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConnectionList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConnectionList whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConnectionList whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|ConnectionList withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ConnectionList withoutTrashed()
 * @mixin \Eloquent
 */
	class ConnectionList extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Country
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Country newQuery()
 * @method static \Illuminate\Database\Query\Builder|Country onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Country withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Country withoutTrashed()
 * @mixin \Eloquent
 */
	class Country extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FriendRequest
 *
 * @property int $id
 * @property int $from_user_id
 * @property int $to_user_id
 * @property int $is_accepted 0=pending,1=accepted,2=declined
 * @property int $type 1=frined request
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest newQuery()
 * @method static \Illuminate\Database\Query\Builder|FriendRequest onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest whereFromUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest whereIsAccepted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest whereToUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|FriendRequest withTrashed()
 * @method static \Illuminate\Database\Query\Builder|FriendRequest withoutTrashed()
 * @mixin \Eloquent
 */
	class FriendRequest extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Language
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Language newQuery()
 * @method static \Illuminate\Database\Query\Builder|Language onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Language query()
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Language withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Language withoutTrashed()
 * @mixin \Eloquent
 */
	class Language extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\LikeDislikes
 *
 * @property int $id
 * @property int $user_id
 * @property int $liked_by_user_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LikeDislikes newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LikeDislikes newQuery()
 * @method static \Illuminate\Database\Query\Builder|LikeDislikes onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|LikeDislikes query()
 * @method static \Illuminate\Database\Eloquent\Builder|LikeDislikes whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LikeDislikes whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LikeDislikes whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LikeDislikes whereLikedByUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LikeDislikes whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LikeDislikes whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|LikeDislikes withTrashed()
 * @method static \Illuminate\Database\Query\Builder|LikeDislikes withoutTrashed()
 * @mixin \Eloquent
 * @property int $is_like 1=liked,2=disliked
 * @method static \Illuminate\Database\Eloquent\Builder|LikeDislikes whereIsLike($value)
 */
	class LikeDislikes extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Notifications
 *
 * @property int $id
 * @property int $from_user_id
 * @property int $to_user_id
 * @property int $type 1=friend_request,2=friend_request_accepted,3=friend_request_declined
 * @property int $is_read 1=yes,0=no
 * @property string|null $notification
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications newQuery()
 * @method static \Illuminate\Database\Query\Builder|Notifications onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications query()
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereFromUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereIsRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereNotification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereToUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notifications whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Notifications withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Notifications withoutTrashed()
 * @mixin \Eloquent
 */
	class Notifications extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Position
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Position newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Position newQuery()
 * @method static \Illuminate\Database\Query\Builder|Position onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Position query()
 * @method static \Illuminate\Database\Eloquent\Builder|Position whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Position whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Position whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Position whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Position whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Position withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Position withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $type 1=mail,2=sub
 * @method static \Illuminate\Database\Eloquent\Builder|Position whereType($value)
 */
	class Position extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Recommendation
 *
 * @property int $id
 * @property int $from_user_id
 * @property int $to_user_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Recommendation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Recommendation newQuery()
 * @method static \Illuminate\Database\Query\Builder|Recommendation onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Recommendation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Recommendation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recommendation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recommendation whereFromUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recommendation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recommendation whereToUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recommendation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Recommendation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Recommendation withoutTrashed()
 * @mixin \Eloquent
 */
	class Recommendation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|Role onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Role withoutTrashed()
 * @mixin \Eloquent
 */
	class Role extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SubPosition
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|SubPosition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubPosition newQuery()
 * @method static \Illuminate\Database\Query\Builder|SubPosition onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|SubPosition query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubPosition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubPosition whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubPosition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubPosition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubPosition whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|SubPosition withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SubPosition withoutTrashed()
 * @mixin \Eloquent
 */
	class SubPosition extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Subscription
 *
 * @property int $id
 * @property int $subscription_plan_id
 * @property int $subscription_plans_details_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereSubscriptionPlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereSubscriptionPlansDetailsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereUserId($value)
 * @mixin \Eloquent
 * @property int $is_active 1=active, 0=inactive
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereIsActive($value)
 * @property int $is_free_plan 1=yes, 0=no
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereIsFreePlan($value)
 * @property string $start_date
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereStartDate($value)
 */
	class Subscription extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SubscriptionPlans
 *
 * @property int $id
 * @property string $plan_name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlans newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlans newQuery()
 * @method static \Illuminate\Database\Query\Builder|SubscriptionPlans onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlans query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlans whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlans whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlans whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlans wherePlanName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlans whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|SubscriptionPlans withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SubscriptionPlans withoutTrashed()
 * @mixin \Eloquent
 */
	class SubscriptionPlans extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SubscriptionPlansDescription
 *
 * @property int $id
 * @property int $subscription_plan_id
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDescription newQuery()
 * @method static \Illuminate\Database\Query\Builder|SubscriptionPlansDescription onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDescription query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDescription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDescription whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDescription whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDescription whereSubscriptionPlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDescription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|SubscriptionPlansDescription withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SubscriptionPlansDescription withoutTrashed()
 * @mixin \Eloquent
 */
	class SubscriptionPlansDescription extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SubscriptionPlansDetails
 *
 * @property int $id
 * @property int $subscription_plan_id
 * @property string $duration
 * @property int $price
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails newQuery()
 * @method static \Illuminate\Database\Query\Builder|SubscriptionPlansDetails onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails whereSubscriptionPlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|SubscriptionPlansDetails withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SubscriptionPlansDetails withoutTrashed()
 * @mixin \Eloquent
 * @property string $time_period 1=weekly,2=monthly,3=yearly,4=days
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails whereTimePeriod($value)
 * @property int $role_id
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlansDetails whereRoleId($value)
 */
	class SubscriptionPlansDetails extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property int $role_id
 * @property string $email
 * @property string|null $password
 * @property int $type 1:normal,2:FB,3:Google,4:Insta,5:Linkedin
 * @property string|null $official_name
 * @property int|null $country_id
 * @property string|null $birth_place
 * @property int|null $city_id
 * @property string|null $city_name
 * @property string|null $date_of_birth
 * @property int|null $age
 * @property string|null $country_origin_id
 * @property string|null $height
 * @property string|null $weight
 * @property string|null $preferred_foot
 * @property int|null $position_id
 * @property int|null $sub_position_id
 * @property string|null $current_club
 * @property string|null $previous_clubs
 * @property string|null $status
 * @property string|null $contract_ends
 * @property string|null $nationality
 * @property string|null $travel_passport
 * @property int|null $language_id
 * @property string|null $phone
 * @property string|null $image
 * @property string|null $last_placed
 * @property string|null $fifa_number
 * @property string|null $club_coach_details
 * @property string|null $club_name
 * @property string|null $division
 * @property string|null $website
 * @property string|null $licence
 * @property int|null $account_verify 1:Verify,2:Un Verify
 * @property int|null $is_verify 1:Verify,2:Un Verify
 * @property string|null $facebook_id
 * @property string|null $google_id
 * @property string|null $instagram_id
 * @property string|null $linkedin_id
 * @property string|null $firebase_id
 * @property string|null $firebase_token
 * @property string|null $device_id
 * @property int|null $device_type 1=Android, 2=iOS
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Achievements[] $achievements
 * @property-read int|null $achievements_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserDocument[] $documents
 * @property-read int|null $documents_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Role|null $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserExperience[] $userExperience
 * @property-read int|null $user_experience_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAccountVerify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBirthPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCityName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereClubCoachDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereClubName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereContractEnds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCountryOriginId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCurrentClub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDateOfBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeviceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDivision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFacebookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFifaNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirebaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirebaseToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGoogleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereInstagramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsVerify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastPlaced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLicence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLinkedinId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereOfficialName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePreferredFoot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePreviousClubs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSubPositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTravelPassport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $compitition
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCompitition($value)
 * @property string|null $phone_code
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneCode($value)
 * @property string|null $summary
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSummary($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserDocument
 *
 * @property int $id
 * @property int $user_id
 * @property string $document
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocument newQuery()
 * @method static \Illuminate\Database\Query\Builder|UserDocument onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocument whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocument whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocument whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDocument whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|UserDocument withTrashed()
 * @method static \Illuminate\Database\Query\Builder|UserDocument withoutTrashed()
 * @mixin \Eloquent
 */
	class UserDocument extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserExperience
 *
 * @property int $id
 * @property int $user_id
 * @property string $position
 * @property string $club_name
 * @property string $location
 * @property string $from_date
 * @property string $to_date
 * @property string $description
 * @property string $media
 * @property string|null $video_link
 * @property string|null $document_link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience newQuery()
 * @method static \Illuminate\Database\Query\Builder|UserExperience onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereClubName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereDocumentLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereFromDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereMedia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereToDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserExperience whereVideoLink($value)
 * @method static \Illuminate\Database\Query\Builder|UserExperience withTrashed()
 * @method static \Illuminate\Database\Query\Builder|UserExperience withoutTrashed()
 * @mixin \Eloquent
 */
	class UserExperience extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\VerifyToken
 *
 * @property int $id
 * @property int $user_id
 * @property int $type 0:Forgot Password, 1:Account verification
 * @property string $token
 * @property string|null $expired_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken newQuery()
 * @method static \Illuminate\Database\Query\Builder|VerifyToken onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken query()
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken whereExpiredDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyToken whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|VerifyToken withTrashed()
 * @method static \Illuminate\Database\Query\Builder|VerifyToken withoutTrashed()
 * @mixin \Eloquent
 */
	class VerifyToken extends \Eloquent {}
}

