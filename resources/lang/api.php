<?php
return [

    'LOGIN' => 'login',
    'REGISTER' => 'Register',
    'SIGN UP' => 'Sign up',
    'FORGOT PASSWORD' => 'Forgot Password',
    'RESET PASSWORD' => 'Reset Password',
    'FEEDBACK' => 'Feedback',
    'REMEMBER ME' => 'Remember me',
    'WELCOME' => 'Welcome',
    'USERNAME' => 'Username',
    'PASSWORD' => 'Password',
    'EMAIL ID' => 'Email id',
    'CONFIRM PASSWORD' => 'Confirm Password',
    'FIRST NAME' => 'First Name',
    'LAST NAME' => 'Last Name',
    'PHONE' => 'Phone',
    'ADDRESS' => 'Address',

]


?>



