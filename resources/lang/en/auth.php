<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
//    FORGOT & RESET PASSWORD
    'InvalidEmail' => 'Email address is not registered with us.',
    'reset_password_link' => 'Reset password link has been sent to your email address.',
    'token_expired' => 'Reset password token has expired. Kindly go through the forgot password procedure again.',
    'invalid_token' => 'Invalid Toked Provided.',
    'reset_successful' => 'Your password has been reset successfully.',
    'something_went_wrong' => 'Something went wrong.',
    'PasswordNotMatch' => 'Email address and password does not match.',
    'PasswordsNotMatch' => 'Password does not match.',
    'PasswordLength' => 'Password must be of minimum 6 characters length.',
    'PasswordRegex' => 'Please enter minimum 6 characters alphanumeric password.',



];
