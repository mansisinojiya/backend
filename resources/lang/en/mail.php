<?php
return [
    'subjects' => [
        'admin' => [
            'forgot_password' => 'Reset Your '.__('admin_api.app_name').' Password',
            'password' => 'Welcome to '.__('admin_api.app_name'),
        ],
        'customer' => [
            'forgot_password' => 'Reset Your '.__('admin_api.app_name').' Password',
        ]
    ],
];