<html>
<head>
    <meta charset="utf-8">
    <title>{{Config::get('app.name')}}</title>
    <style type="text/css">
        html,
        body {
            margin: 0;
            padding: 0;
            height: 100%;
        }

        body {
            font-family: Helvetica, Arial, Sans-Serif;
            background-color: #FFF;
            color: #333;
            -moz-font-smoothing: antialiased;
            -webkit-font-smoothing: antialiased;
        }

        .error-container {
            text-align: center;
            height: 100%;
        }

        @media (max-width: 480px) {
            .error-container {
                position: relative;
                top: 50%;
                height: initial;
                -webkit-transform: translateY(-50%);
                -ms-transform: translateY(-50%);
                transform: translateY(-50%);
            }
        }

        .error-container h1 {
            margin: 0;
            font-size: 130px;
            font-weight: 300;
        }

        @media (min-width: 480px) {
            .error-container h1 {
                position: relative;
                top: 50%;
                -webkit-transform: translateY(-50%);
                -ms-transform: translateY(-50%);
                transform: translateY(-50%);
            }
        }

        @media (min-width: 768px) {
            .error-container h1 {
                font-size: 220px;
            }
        }

        .return {
            color: #333;
            font-weight: 400;
            letter-spacing: -0.04em;
            margin: 0;
        }

        @media (min-width: 480px) {
            .return {
                position: absolute;
                width: 100%;
                bottom: 30px;
            }
        }

        .return a {
            padding-bottom: 1px;
            color: #333;
            text-decoration: none;
            border-bottom: 1px solid rgba(0, 0, 0, 0.6);
            -webkit-transition: border-color 0.1s ease-in;
            transition: border-color 0.1s ease-in;
        }

        .return a:hover {
            border-bottom-color: #333;
        }
    </style>
</head>

<body>

<div class="error-container">
    <img style="margin-top: 10%;" src="{{url('web-assets/images/logo.png')}}" alt="{{getenv('APP_NAME')}}" width="125">
    <h2 style="font-size: 50px; margin-top: 1%;">To use {{getenv('APP_NAME')}}, please enable JavaScript.</h2>
    <br>
    <h4><p class="return">Take me back to <a href="/moosa">{{ Config::get('app.name') }}</a></p></h4>
</div>

</body>
</html>
