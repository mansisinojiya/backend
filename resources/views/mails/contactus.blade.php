@extends('mails.emailMaster')
@section('content')
    <tr>
        <td class="bg_white email-section">
            <div class="heading-section" style="text-align: left; padding: 0;">
                <h2 style="margin-bottom: 0; font-weight: 600; font-size: 16px; margin-bottom: 0.5em">Hello Admin,</h2>
                    <p style="margin-top: 0; font-size: 14px;">Please check below user details.</p>
                    <p><b>Name:</b> {{$user->name}}</p>
                    <p><b>Email:</b> {{$user->email}}</p>
                    <p><b>Phone Number:</b> {{$user->mobile}}</p>
                    @if ($user->message)
                    <p><b>Message:</b> {{$user->message}}</p>
                    @endif
                <p style="font-size: 18px; margin-top: 3em; margin-bottom: 0; line-height: 28px; color: #333;">Thank you, <span style="display: block; color: #ee323e; font-weight: 400;">Team {{ Config::get('app.name') }}.</span></p>
            </div>
        </td>
    </tr><!-- end: tr -->
@endsection
