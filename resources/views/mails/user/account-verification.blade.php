@extends('mails.emailMaster')
@section('content')
    <tr>
        <td class="bg_white email-section">
            <div class="heading-section" style="text-align: left; padding: 0;">
                <h2 style="margin-bottom: 0; font-weight: 600; font-size: 16px; margin-bottom: 0.5em">Hi
                    <b>{!! ucfirst($name)  !!}
                    </b>,</h2>
                <p style="margin-top: 0; font-size: 14px;">Below is the link
                    to verify your account</p>
                <div style="text-align: center">
                    <a style="display: inline-block; font-size: 18px;
                    line-height: 56px; text-align: center; margin: 2em auto
                    0; color: #000000; height: 56px; background-color: #FFF;
                    border: 1px solid #E8BF7D; padding: 0 24px"
                       href="{{$link}}"><span style="color:
                       #000">Verify</span></a>
                </div>
                <p style="font-size: 18px; margin-top: 3em; margin-bottom: 0; line-height: 28px; color: #333;">Thank you, <span style="display: block; color: #ee323e; font-weight: 400;">Team {{ Config::get('app.name') }}.</span></p>
            </div>
        </td>
    </tr><!-- end: tr -->
@endsection
