@extends('mails.emailMaster')
@section('content')
    <tr>
        <td class="bg_white email-section">
            <div class="heading-section" style="text-align: left; padding: 0;">
                <h2 style="margin-bottom: 0; font-weight: 600; font-size: 16px; margin-bottom: 0.5em">Hi
                    <b>{!! ucfirst( $status == 1 ? $user->UserDetail->first_name." ".$user->UserDetail->last_name : $user->UserDetail->secondary_first_name." ".$user->UserDetail->secondary_last_name)   !!}</b>,</h2>
                <p style="margin-top: 0; font-size: 14px;">{{ __('mail.messages.verification_code_content') }}</p>
                <div style="text-align: center">
                    <a style="display: inline-block; font-size: 18px;line-height: 56px; text-align: center; margin: 2em auto 0; color: #000000; height: 56px; background-color: #ffeeef; border: 1px solid #ed1c24; padding: 0 24px" href="#">Verification Code : <span style="color: #ee323e">{{ $status == 1 ? $user->verify_code :  $user->secondary_verify_code }}</span></a>
                </div>
                <p style="font-size: 18px; margin-top: 3em; margin-bottom: 0; line-height: 28px; color: #333;">{{$content}}</p>
                <p style="font-size: 18px; margin-top: 3em; margin-bottom: 0; line-height: 28px; color: #333;">Thank you, <span style="display: block; color: #ee323e; font-weight: 400;">Team {{ Config::get('app.name') }}.</span></p>
            </div>
        </td>
    </tr><!-- end: tr -->
@endsection
