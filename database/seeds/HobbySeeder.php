<?php
use Illuminate\Database\Seeder;

class HobbySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('hobby')->insert([
            [
                'hobby' => 'spots',
            ],
            [
                'hobby' => 'reading',
            ],
            [
                'hobby' => 'outing',
            ],
            [
                'hobby' => 'travelling',
            ],
            [
                'hobby' => 'singing',
            ],
        ]);
    }
}
