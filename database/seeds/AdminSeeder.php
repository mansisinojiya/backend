<?php
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = \App\Models\Admin::findOrNew(1);
        $admin->email = 'admin@mailinator.com';
        $admin->password = bcrypt('admin@123');
        $admin->first_name = 'Super';
        $admin->last_name = 'Admin';
        $admin->mobile = 1234567890;
        $admin->save();
    }
}
