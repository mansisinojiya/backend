<?php
use Illuminate\Database\Seeder;
class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('city')->insert([
            [
                'city_name' => 'ahmedabad',
            ],
            [
                'city_name' => 'baroda',
            ],
            [
                'city_name' => 'rajkot',
            ],
            [
                'city_name' => 'surat',
            ],
            [
                'city_name' => 'gandhinagar',
            ],
        ]);
    }
}
